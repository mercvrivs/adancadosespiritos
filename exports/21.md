# Paradigmas #6 - Fantasias

VAMOS DIZER POR ASSIM, que fantasias são as roupagens que eu a minha garota gostamos de usar para nos divertir.

MAS COMO NÓS, eu e a minha garotinha somos Brujitos sin señor, assim como meus amigos e as suas garotiñas tambien, as fantasias que nós usamos para nos divertir no mundo dos homens são un poquito diferentes, quero dizer, de uma natureza um pouquinho diferente.

Além disso, nós costumamos trocá-las aqui e acolá.

À medida do nosso divertimento, do nosso riso e da nossa alegria.

Divertimento, *scherzo*, fantasia, improviso. Allegro ma non tanto. Allegro con fuoco!

> “Odor de cujo corpo embriaga a alma, luz de cuja alma rebaixa esse corpo às feras!“ ─ Liber LXV

