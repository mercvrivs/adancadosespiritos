# Tecnologia Mágicka #1 - Áreas de Acesso Difícil

## Acessando Áreas Astrais Intrincadas

> A técnica usada pelo Iniciado para criar realinhamentos e acessar regiões astrais de acesso difícil ou perigoso, e realinhar passo-atrás ou passo-além.

Existem dois tipos básicos destas áreas, a primeira é *humana*, a segunda é *não-humana*. É adequado encaixar hominídeos nas áreas *humanas*, porque é evidente.

