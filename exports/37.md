# Movimentos #3 - Breaking Through

*Breaking through* é o movimento de Acessar.

I. Aprende a flexionar a mente como corrente alternada. Isso se refere a passar a mente por todas as coisas sem se prender a nada. (*Passing through* ─ Dissolver)

II. Aprende a parar a mente como cavalo de pau. (*Holding*, ou *Grasping* ─ Parar, ou Endurecer)

III. Treina, treina, te torna hábil, na arte de quebrar pro outro lado.

