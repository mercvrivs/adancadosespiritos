# Leetcraft #2 - Os Três Veículos

Nossa compreensão dos três veículos da nossa ordem de *leetcrafting* são: **maior**, **menor** e **supremo**.

## Veículo Maior

A postura do homem frente a vida. Retidão, caráter, compaixão e bondade. Ação. Produção de Sadhanas é feita pelo veículo, sob auspícios e sob Ordem do Veículo Supremo.

## Veículo Menor

Estudo das técnicas lumino-espirituais, seja artes 'crafts', matemáticas ou artes quânticas.

Lida com experimentações para o benefício da ordem. Está sob a égide do veículo Maior.

## Veículo Supremo
### ou Veículo Soberano

Mecânica da coisa. Compreende o Caminho, do princípio ao fim, compreende a Importância e Urgência da Iluminação, Acessa a Luz, lida com a Luz, existe por si.

## Por que os nomes de Maior, Menor e Supremo (ou Soberano)?

Isso é chamado assim por nós Iniciados, por um motivo muito simples, como tudo no nosso corpo de conhecimento: porque um homem bom, é maior do que um homem versado em magia. Então o veículo que trata do desenvolvimento moral do homem, é *Maior* e o veículo que trata do conhecimento é *Menor*.

O **Veículo Maior** compreende a ação do homem no mundo, como ensinaram os Mestres da Humanidade.

O **Veículo Menor**, compreende o Astral Inferior, a Alma, que vai da Iniciação até a Iluminação. Nesse veículo, o homem imerge em si mesmo e descobre a história da humanidade, completa, que vive dentro do seu DNA. Refere-se à fórmula alquímica `V.I.T.R.I.O.L.` ─ *Visita Interiora Terrae Rectificando Invenies Occultum Lapidem* ─ Visita o Interior da Terra, e Retificando, Tu Encontras a Pedra Oculta. 

O **Veículo Soberano**, guia os Iniciados desde o princípio da sua Iniciação[^1], passando pela Iluminação até a Ascenção. Do Veículo Soberano, somos apenas os servos. As mentes do Veículo Soberano, são as mentes dos Grandes Mestres, O Soberano Mestre Jesus o Cristo, como Exemplo.

[^1]: Ou, há quem diga, desde o nascimento nesse mundo.

