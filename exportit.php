#!/usr/bin/env php
<?php

error_reporting(E_ALL);

$file = file_get_contents("posts.csv");
//$array = str_getcsv($file,',','"',''); //WORKING
$array = str_getcsv($file);

// var_dump($array);
//print_r($array);
/*
    [1] => id
    [2] => uuid
    [3] => title
    [4] => slug
    [5] => markdown
    [6] => html
    [7] => image
    [8] => featured
    [9] => page
    [10] => status
    [11] => language
    [12] => meta_title
    [13] => meta_description
    [14] => author_id
    [15] => created_at
    [16] => created_by
    [17] => updated_at
    [18] => updated_by
    [19] => published_at
    [20] => published_by

*/

$arr_size = count($array);

$c = 1;

$n = 0;

$pandoc =<<<TEXT
#!/bin/bash

pandoc -S --template=default.latex --latex-engine=xelatex --toc --toc-depth=6 -o adancadosespiritos.pdf title.txt \\

TEXT;

for ($i=0; $i < $arr_size; $i+=19)
{
	$n++;

	//if(!$array[$i]) {$i++; continue;}
	for($c=1; $c<=19; $c++)
	{

	//	if(!$array[$i+$c]) {continue;}

		switch($c)
		{
			case 2:
				$title = $array[$i+$c];
				break;

			case 4:
				$markdown = $array[$i+$c];
				break;

			case 8:
				$page = $array[$i+$c];
				$page += 0;
				break;

			case 9:
				$published = $array[$i+$c];
				break;
	
			case 14:
				$time = floatval($array[$i+$c]);
				//$time += 0; //convert to long
				$date = date("r", $time);

				//$date=$time;
				break;
		}
	}

/*	if($c==20)
	{
		$c = 1;
		$n++; // next post
 */		
	$output=<<<TEXT
# $title

$markdown


TEXT;

	if(strcmp($published,"published")==0 && $page!=1) {
		$filename = "exports/{$n}.md";
		file_put_contents($filename, $output);
		//
		//echo "\n\nNUMBER: {$n}\n\n" . $output . "\n\n";
		$pandoc .= "{$filename} \\\n";
	}

	unset($title, $markdown, $time, $date, $published, $page, $filename);

/*	}
	else
	{
		$c++;
	}*/
}

/*$pandoc .=<<<TEXT
end-metadata.md \\

	TEXT;*/

echo $pandoc;
file_put_contents("makebook.sh", $pandoc);
chmod("makebook.sh", 0777);

echo "\n\nExecuting makebook.sh ...\n\n";
system("./makebook.sh");

?>
